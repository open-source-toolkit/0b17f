# cri-dockerd RPM 包下载

## 资源文件介绍

本仓库提供了一个用于下载的资源文件：`cri-dockerd-0.3.4-3.el7.x86-64.rpm`。

### 文件描述

`cri-dockerd-0.3.4-3.el7.x86-64.rpm` 是一个适用于 CentOS 7 系统的 RPM 包，版本号为 0.3.4-3。该 RPM 包包含了 `cri-dockerd` 的二进制文件及相关依赖，方便用户在 CentOS 7 系统上快速安装和配置 `cri-dockerd`。

### 使用说明

1. **下载文件**：
   - 您可以直接从本仓库下载 `cri-dockerd-0.3.4-3.el7.x86-64.rpm` 文件。

2. **安装 RPM 包**：
   - 下载完成后，您可以使用以下命令在 CentOS 7 系统上安装该 RPM 包：
     ```bash
     sudo rpm -ivh cri-dockerd-0.3.4-3.el7.x86-64.rpm
     ```

3. **验证安装**：
   - 安装完成后，您可以通过以下命令验证 `cri-dockerd` 是否成功安装：
     ```bash
     cri-dockerd --version
     ```

### 注意事项

- 请确保您的系统是 CentOS 7 并且是 x86_64 架构。
- 在安装之前，建议您备份重要数据，以防万一。

### 联系我们

如果您在使用过程中遇到任何问题或有任何建议，欢迎通过以下方式联系我们：
- 邮箱：example@example.com
- 电话：123-456-7890

感谢您的使用！